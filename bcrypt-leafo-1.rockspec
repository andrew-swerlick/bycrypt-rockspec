package = "bcrypt"
version = "leafo-1"

source = {
	url = "git://github.com/leafo/luabcrypt.git",
	branch = "latest"
}

description = {
	summary = "A Lua wrapper for bcrypt",
	homepage = "http://github.com/mikejsavage/lua-bcrypt",
	license = "MIT",
	maintainer = "Mike Savage",
}

dependencies = {
	"lua >= 5.1",
}

build = {
	type = "builtin",
	modules = {
		bcrypt = {
			sources = {
				"lib/bcrypt/crypt_blowfish.c",
				"lib/bcrypt/crypt_gensalt.c",
				"lib/bcrypt/wrapper.c",
				"src/main.c",
			},
			incdirs = {
				"lib/bcrypt/"
			}
		}
	}
}
